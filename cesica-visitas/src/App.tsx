import Login from "./Views/Login/Login";
import RecoveryPass from "./Components/RecoveryPass/RecoveryPass";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./Components/Layout";
import "./App.css";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Login />} />
            <Route path="/recovery" element={<RecoveryPass />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
