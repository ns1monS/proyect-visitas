import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./RecoveryPass.css";

export default function RecoveryPass() {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (password !== confirmPassword) {
      setError("Las contraseñas no coinciden.");
    } else {
      alert("Contraseña actualizada correctamente.");
    }
  };

  const handleForgotPassword = () => {
    console.log("Forgot Password clicked");
  };
  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="password" className="input-label">
        Contraseña:
      </label>
      <input
        type="password"
        id="password"
        name="password"
        value={password}
        onChange={(event) => setPassword(event.target.value)}
        className="input-field"
        required
      />

      <div className="password-input-container">
        <label htmlFor="confirmPassword" className="input-label">
          Confirmar Contraseña:
        </label>
        <input
          type="password"
          id="confirmPassword"
          name="password"
          value={confirmPassword}
          onChange={(event) => setConfirmPassword(event.target.value)}
          className="input-field password-input"
          required
        />
        <div className="container">
          <Link to={"/"}>
            <button
              type="button"
              className="forgot-password-button"
              onClick={handleForgotPassword}
            >
              Volver
            </button>
          </Link>
        </div>
      </div>
      {error && <p className="error">{error}</p>}
      <button type="submit" className="submit-button">
        Actualizar contraseña
      </button>
    </form>
  );
}
