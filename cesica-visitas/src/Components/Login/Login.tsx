import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Login.css";

export default function LoginForm() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };

  const handleForgotPassword = () => {
    console.log("Forgot Password clicked");
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="username" className="input-label">
        Usuario:
      </label>
      <input
        type="text"
        id="username"
        name="username"
        value={username}
        onChange={(event) => setUsername(event.target.value)}
        className="input-field"
      />

      <div className="password-input-container">
        <label htmlFor="password" className="input-label">
          Contraseña:
        </label>
        <input
          type="password"
          id="password"
          name="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          className="input-field password-input"
        />
        <div className="container">
          <Link to={"/recovery"}>
            <button
              type="button"
              className="forgot-password-button"
              onClick={handleForgotPassword}
            >
              Olvidé mi contraseña
            </button>
          </Link>
        </div>
      </div>

      <button type="submit" className="submit-button">
        Aceptar
      </button>
    </form>
  );
}
