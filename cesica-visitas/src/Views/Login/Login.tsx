import LoginForm from "../../Components/Login/Login";
import "./Login.css";
export default function Login() {
  return (
    <>
      <div className="container">
        <LoginForm />
      </div>
    </>
  );
}
